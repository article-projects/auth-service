/* eslint-disable global-require */
global.console = { log: jest.fn(), error: jest.fn() };

const restify = require('restify');

describe('Auth service unit tests', () => {
  let server = null;

  afterAll(() => {
    server.close();
  });

  test('Should create and start Restify', (done) => {
    const spyCreate = jest.spyOn(restify, 'createServer');
    server = require('./index.js');
    server.on('ServerStarted', () => {
      expect(spyCreate).toHaveBeenCalled();
      done();
    });
  });
});
