module.exports = {
  up: queryInterface => (
    queryInterface.bulkInsert('Users', [{
      username: 'FirstUser',
      passwordHash: '$2y$12$M9D5R21X.6TJR8WJ66af6eUyBfYwVgjJibcOcwhptvfp6a8.46yD6', // "Password1"
      updatedAt: new Date(),
      createdAt: new Date(),
    }], {})
  ),
  down: queryInterface => (
    queryInterface.bulkDelete('Users', {
      username: ['FirstUser'],
    })
  ),
};
