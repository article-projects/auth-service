const app = require('./app/app');
const nconf = require('nconf');

app.listen(nconf.get('http:port'), () => {
  console.log(`Server running on: ${JSON.stringify(app.address(), undefined, 2)}`);
  console.log(`Server environment: ${nconf.get('NODE_ENV')}`);
  app.emit('ServerStarted');
});

module.exports = app;
