const defaultValues = {
  http: {
    port: 3100,
  },
  jwt: {
    accessToken: {
      expiration: 1800, // 1800 = 60 * 60 * 24 * 8 is 30 minutes
      audience: 'https://app.example.com',
      issuer: 'https://auth.example.com',
    },
    refreshToken: {
      expiration: 2592000, // 2592000 = 60 * 60 * 24 * 30 is 30 days
      audience: 'https://auth.example.com',
      issuer: 'https://auth.example.com',
    },
  },
  cookie: {
    name: 'refreshToken',
    path: '/protected',
    maxAge: 2592000, // 2592000 = 60 * 60 * 24 * 30 is 30 days
    secure: true,
    httpOnly: true,
    sameSite: 'Strict',
  },
};

module.exports = defaultValues;
