const requiredValues = [
  'NODE_ENV',
  'http:port',
  'http:allowedOrigins',
  'jwt:accessToken:secret',
  'jwt:accessToken:expiration',
  'jwt:accessToken:audience',
  'jwt:accessToken:issuer',
  'jwt:refreshToken:secret',
  'jwt:refreshToken:expiration',
  'jwt:refreshToken:audience',
  'jwt:refreshToken:issuer',
  'cookie:path',
  'cookie:maxAge',
  'cookie:secure',
  'cookie:httpOnly',
  'cookie:sameSite',
];

module.exports = requiredValues;
