const restify = require('restify');
const helmet = require('helmet');
const corsMiddleware = require('restify-cors-middleware');
const nconf = require('nconf');
require('./lib/config')();

const cors = corsMiddleware({
  preflightMaxAge: 5,
  origins: nconf.get('http:allowedOrigins').split(','),
  credentials: true,
});
const app = restify.createServer({ name: 'Auth Service' });
app.use(helmet({
  contentSecurityPolicy: { directives: { defaultSrc: ["'self'"] } },
  referrerPolicy: { policy: 'same-origin' },
}));
app.pre(cors.preflight);
app.use(cors.actual);
app.use(restify.plugins.acceptParser(app.acceptable));
app.use(restify.plugins.bodyParser({ mapParams: true }));
app.use(restify.plugins.throttle({
  burst: 50,
  rate: 20,
  ip: true,
}));

require('./routes')(app);

module.exports = app;
