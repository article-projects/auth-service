const restifyAsyncWrap = require('@gilbertco/restify-async-wrap');
const getHealthCheck = require('./get');

module.exports = (server) => {
  server.get('/', restifyAsyncWrap(getHealthCheck));
};
