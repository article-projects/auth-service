const db = require('../../../models/sequelize');

const getHealthCheck = async (req, res, next) => {
  await db.sequelize.authenticate();
  res.send(200);
  return next();
};

module.exports = getHealthCheck;
