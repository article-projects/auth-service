const request = require('supertest');
const db = require('../../../models/sequelize');
const app = require('../../app');

describe('Get / API tests', () => {
  afterAll(() => {
    app.close();
    db.sequelize.close();
  });

  test('Should return success for #get /', async () => {
    const response = await request.agent(app)
      .get('/');
    expect(response.statusCode).toBe(200);
  });
});
