/* eslint-disable global-require */

module.exports = function (server) {
  require('./root')(server);
  require('./login')(server);
  require('./protected')(server);
};
