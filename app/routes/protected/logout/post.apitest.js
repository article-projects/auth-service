const request = require('supertest');
const db = require('../../../../models/sequelize');
const app = require('../../../app');
const nconf = require('nconf');

// Created in https://jwt.io/
const validToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJodHRwczovL2F1dGguZXhhbXBsZS5jb20iLCJpc3MiOiJodHRwczovL2F1dGguZXhhbXBsZS5jb20iLCJzdWIiOjEsImlhdCI6MTUyODAzNDk4NCwiZXhwIjoxODMxNTk5MDIyfQ.bgZHE0TZPunkx00XwqbGjWfVrnyu3xxS2_U7cZd4pNc';

describe('Post /logout API tests', () => {
  afterAll(() => {
    app.close();
    db.sequelize.close();
  });

  test('Should return ok and clear cookie & token for #post /logout with valid refresh token', async () => {
    const response = await request.agent(app)
      .post('/protected/logout')
      .set('Cookie', `${nconf.get('cookie:name')}=${validToken}`);
    expect(response.statusCode).toBe(200);
    expect(response.body.accessToken).toBe('');
    expect(response.header['set-cookie'][0]).toBe(`${nconf.get('cookie:name')}=; Max-Age=0; Path=/protected; HttpOnly; Secure; SameSite=Strict`);
  });

  test('Should return ok and clear cookie & token for #post /logout without refresh token', async () => {
    const response = await request.agent(app)
      .post('/protected/logout');
    expect(response.statusCode).toBe(200);
    expect(response.body.accessToken).toBe('');
  });
});
