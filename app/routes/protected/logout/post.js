const refreshToken = require('../../../lib/token/refreshToken');
const cookie = require('cookie');

const postLogout = async (req, res, next) => {
  res.setHeader(
    'Set-Cookie',
    cookie.serialize(
      refreshToken.cookieName,
      '',
      refreshToken.clearCookieConfig
    )
  );
  res.send(200, { accessToken: '' });
  return next();
};

module.exports = postLogout;
