/* eslint-disable global-require */

module.exports = function (server) {
  require('./logout')(server);
  require('./renew')(server);
};
