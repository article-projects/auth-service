const jwt = require('restify-jwt-community');
const nconf = require('nconf');
const restifyAsyncWrap = require('@gilbertco/restify-async-wrap');
const tokenFromCookie = require('../../../lib/token/tokenFromCookie');
const getRenew = require('./get');

module.exports = (server) => {
  server.get(
    '/protected/renew',
    jwt({
      algorithms: ['HS256'],
      audience: nconf.get('jwt:refreshToken:audience'),
      issuer: nconf.get('jwt:refreshToken:issuer'),
      secret: nconf.get('jwt:refreshToken:secret'),
      getToken: tokenFromCookie,
    }),
    restifyAsyncWrap(getRenew));
};
