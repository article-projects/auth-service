const request = require('supertest');
const db = require('../../../../models/sequelize');
const app = require('../../../app');
const nconf = require('nconf');

// Created in https://jwt.io/
const validToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJodHRwczovL2F1dGguZXhhbXBsZS5jb20iLCJpc3MiOiJodHRwczovL2F1dGguZXhhbXBsZS5jb20iLCJzdWIiOjEsImlhdCI6MTUyODAzNDk4NCwiZXhwIjoxODMxNTk5MDIyfQ.bgZHE0TZPunkx00XwqbGjWfVrnyu3xxS2_U7cZd4pNc';
const invalidSubToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJodHRwczovL2F1dGguZXhhbXBsZS5jb20iLCJpc3MiOiJodHRwczovL2F1dGguZXhhbXBsZS5jb20iLCJzdWIiOjEyMzQ1LCJpYXQiOjE1MjgwMzQ5ODQsImV4cCI6MTgzMTU5OTAyMn0.HXhupEjQnsTOysI259z7-Y06l7YeKYX0pwzp1ayQjQI';
const invalidAudienceToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJodHRwczovL2F1dGguZXhhbXBsZWUuY29tIiwiaXNzIjoiaHR0cHM6Ly9hdXRoLmV4YW1wbGUuY29tIiwic3ViIjoxLCJpYXQiOjE1MjgwMzQ5ODQsImV4cCI6MTgzMTU5OTAyMn0.8l3a56QeCIG1sZMpuqvfReVIXp6UO7Q-ybTgw572ozo';
const invalidIssuerToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJodHRwczovL2F1dGguZXhhbXBsZS5jb20iLCJpc3MiOiJodHRwczovL2F1dGguZXhhbXBsZWUuY29tIiwic3ViIjoxLCJpYXQiOjE1MjgwMzQ5ODQsImV4cCI6MTgzMTU5OTAyMn0.yKRj3dyIgPSmg86aVV0zX4-8j12pzUE0KNZCyOhcIMw';
const invalidNoneAlgToken = 'eyJhbGciOiJub25lIiwidHlwIjoiSldUIn0.eyJhdWQiOiJodHRwczovL2F1dGguZXhhbXBsZS5jb20iLCJpc3MiOiJodHRwczovL2F1dGguZXhhbXBsZWUuY29tIiwic3ViIjoxLCJpYXQiOjE1MjgwMzQ5ODQsImV4cCI6MTgzMTU5OTAyMn0.';
const invalidHS384AlgToken = 'eyJhbGciOiJIUzM4NCIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJodHRwczovL2F1dGguZXhhbXBsZS5jb20iLCJpc3MiOiJodHRwczovL2F1dGguZXhhbXBsZWUuY29tIiwic3ViIjoxLCJpYXQiOjE1MjgwMzQ5ODQsImV4cCI6MTgzMTU5OTAyMn0.2aood-SQEIefvNDvV_AICl4Umw-sCgs3kGFM1sVz6Sap6q1T4_zbMDQgdMq4vzKe';

describe('Get /renew API tests', () => {
  afterAll(() => {
    app.close();
    db.sequelize.close();
  });

  test('Should return a client error for #get /renew without proper refresh token', done => (
    request(app)
    .get('/protected/renew')
    .expect(401, {
      code: 'InvalidCredentials',
      message: 'No authorization token was found',
    })
    .end((err) => {
      if (err) return done.fail(err);
      return done();
    })
  ));

  test('Should return a client error for #get /renew with non-existing user id (sub) in the token', async () => {
    const response = await request.agent(app)
      .get('/protected/renew')
      .set('Cookie', `${nconf.get('cookie:name')}=${invalidSubToken}`);
    expect(response.statusCode).toBe(401);
    expect(response.body.code).toBe('InvalidCredentials');
  });

  test('Should return a new access token for #get /renew with proper refresh token', async () => {
    const response = await request.agent(app)
      .get('/protected/renew')
      .set('Cookie', `${nconf.get('cookie:name')}=${validToken}`);
    expect(response.statusCode).toBe(200);
    expect(response.body.accessToken.length).toBeGreaterThan(20);
  });

  test('Should return a client error for #get /renew with invalid issuer field in token', async () => {
    const response = await request.agent(app)
      .get('/protected/renew')
      .set('Cookie', `${nconf.get('cookie:name')}=${invalidIssuerToken}`);
    expect(response.statusCode).toBe(401);
    expect(response.body.code).toBe('InvalidCredentials');
  });

  test('Should return a client error for #get /renew with invalid audience field in token', async () => {
    const response = await request.agent(app)
      .get('/protected/renew')
      .set('Cookie', `${nconf.get('cookie:name')}=${invalidAudienceToken}`);
    expect(response.statusCode).toBe(401);
    expect(response.body.code).toBe('InvalidCredentials');
  });

  test('Should return a client error for #get /renew with "HS384" algorithm field in the token', async () => {
    const response = await request.agent(app)
      .get('/protected/renew')
      .set('Cookie', `${nconf.get('cookie:name')}=${invalidHS384AlgToken}`);
    expect(response.statusCode).toBe(401);
    expect(response.body.code).toBe('InvalidCredentials');
  });

  test('Should return a client error for #get /renew with "none" algorithm field in the token', async () => {
    const response = await request.agent(app)
      .get('/protected/renew')
      .set('Cookie', `${nconf.get('cookie:name')}=${invalidNoneAlgToken}`);
    expect(response.statusCode).toBe(401);
    expect(response.body.code).toBe('InvalidCredentials');
  });
});
