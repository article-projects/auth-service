const errors = require('restify-errors');
const db = require('../../../../models/sequelize');
const accessToken = require('../../../lib/token/accessToken');

const getRenew = async (req, res, next) => {
  let user;
  try {
    user = await db.User.findById(req.user.sub);
  } catch (e) {
    return next(new errors.InternalServerError(e));
  }

  if (user === null) {
    return next(new errors.InvalidCredentialsError());
  }

  res.send(200, { accessToken: accessToken.createAccessToken(user) });
  return next();
};

module.exports = getRenew;
