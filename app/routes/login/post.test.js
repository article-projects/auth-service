/* eslint-disable global-require */
const errors = require('restify-errors');

jest.mock('../../../models/sequelize');
const db = require('../../../models/sequelize');

describe('Post /login unit tests', () => {
  test('Should handle db exceptions', async () => {
    const mockRes = jest.fn();
    const mockNext = jest.fn();
    db.User.findOne = jest.fn(() => Promise.reject('I reject!'));
    const postLogin = require('./post.js');
    await postLogin(
      { params: { username: 'abba' } },
      mockRes,
      mockNext
    );
    expect(mockRes).toHaveBeenCalledTimes(0);
    expect(mockNext).toHaveBeenCalledTimes(1);
    expect(mockNext.mock.calls[0][0]).toEqual(new errors.InternalServerError('I reject!'));
  });
});
