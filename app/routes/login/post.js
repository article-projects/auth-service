const errors = require('restify-errors');
const bcrypt = require('bcryptjs');
const cookie = require('cookie');
const { validationResult } = require('express-validator/check');
const db = require('../../../models/sequelize');
const refreshToken = require('../../lib/token/refreshToken');

const postLogin = async (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next(new errors.UnprocessableEntityError());
  }
  let user;
  try {
    user = await db.User.findOne({
      where: { username: req.params.username },
    });
  } catch (e) {
    return next(new errors.InternalServerError(e));
  }

  if (user === null) {
    return next(new errors.InvalidCredentialsError());
  }

  if (bcrypt.compareSync(req.params.password, user.get('passwordHash'))) {
    res.setHeader(
      'Set-Cookie',
      cookie.serialize(
        refreshToken.cookieName,
        refreshToken.createRefreshToken(user),
        refreshToken.cookieConfig
      )
    );
    res.send(200);
    return next();
  }
  return next(new errors.InvalidCredentialsError());
};

module.exports = postLogin;
