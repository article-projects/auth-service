/* eslint-disable global-require */
const request = require('supertest');
const nconf = require('nconf');
const cookie = require('cookie');
const db = require('../../../models/sequelize');
const app = require('../../app');

describe('Post /login API tests', () => {
  afterAll(() => {
    app.close();
    db.sequelize.close();
  });

  test('Should return a client error for #post /login with wrong username', async () => {
    const response = await request(app)
      .post('/login')
      .send({ username: 'InvalidUser', password: 'Password1' });
    expect(response.statusCode).toBe(401);
    expect(response.body.code).toBe('InvalidCredentials');
  });

  test('Should return a client error for #post /login with wrong password', async () => {
    const response = await request(app)
      .post('/login')
      .send({ username: 'FirstUser', password: 'WrongPassword' });
    expect(response.statusCode).toBe(401);
    expect(response.body.code).toBe('InvalidCredentials');
  });

  test('Should return ok for #post /login with correct credentials', async () => {
    const response = await request(app)
      .post('/login')
      .send({ username: 'FirstUser', password: 'Password1' });
    expect(response.statusCode).toBe(200);
    const cookies = cookie.parse(response.header['set-cookie'][0]);
    expect(cookies[nconf.get('cookie:name')].length).toBeGreaterThan(20);
    expect(cookies['Max-Age']).toBe(nconf.get('cookie:maxAge').toString());
    expect(cookies.Path).toBe(nconf.get('cookie:path'));
    expect(cookies.SameSite).toBe(nconf.get('cookie:sameSite'));
  });

  test('Should return error for #post /login with too short username', async () => {
    const response = await request(app)
      .post('/login')
      .send({ username: 'F', password: 'Password1' });
    expect(response.statusCode).toBe(422);
  });

  test('Should return error for #post /login with too long username', async () => {
    const response = await request(app)
      .post('/login')
      .send({ username: 'fggagdngnfkgn.fgfnfndca.jfkljfkmfnrjcgfmrbngmhmbghd', password: 'Password1' });
    expect(response.statusCode).toBe(422);
  });

  test('Should return error for #post /login with too short password', async () => {
    const response = await request(app)
      .post('/login')
      .send({ username: 'FirstUser', password: 'Passwo1' });
    expect(response.statusCode).toBe(422);
  });

  test('Should return error for #post /login with too long password', async () => {
    const response = await request(app)
      .post('/login')
      .send({ username: 'FirstUser', password: 'Password1fdjghkjghsfjghjsfhgkhfdgrhggsfdgrgfdggsgad' });
    expect(response.statusCode).toBe(422);
  });
});
