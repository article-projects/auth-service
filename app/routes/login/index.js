const restifyAsyncWrap = require('@gilbertco/restify-async-wrap');
const { check } = require('express-validator/check');
const postLogin = require('./post');

module.exports = (server) => {
  server.post('/login', [
    check('username')
      .isLength({ min: 2, max: 50 }),
    check('password')
      .isLength({ min: 8, max: 50 }),
  ], restifyAsyncWrap(postLogin));
};
