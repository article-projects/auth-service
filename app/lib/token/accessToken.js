const jsonwt = require('jsonwebtoken');
const nconf = require('nconf');

const createAccessToken = (user) => {
  const iatTime = Math.floor(Date.now() / 1000);
  const expTime = iatTime + nconf.get('jwt:accessToken:expiration');

  const payload = {
    aud: nconf.get('jwt:accessToken:audience'),
    iss: nconf.get('jwt:accessToken:issuer'),
    sub: user.get('id'),
    iat: iatTime,
    exp: expTime,
  };
  const options = {
    algorithm: 'HS256',
  };
  return jsonwt.sign(payload, nconf.get('jwt:accessToken:secret'), options);
};

module.exports = {
  createAccessToken,
};
