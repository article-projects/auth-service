const nconf = require('nconf');
const cookie = require('cookie');

const tokenFromCookie = (req) => {
  const cookies = cookie.parse(req.headers.cookie || '');
  if (cookies && cookies[nconf.get('cookie:name')]) {
    return cookies[nconf.get('cookie:name')];
  }
  return null;
};

module.exports = tokenFromCookie;
