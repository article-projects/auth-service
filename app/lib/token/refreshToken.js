const jsonwt = require('jsonwebtoken');
const nconf = require('nconf');

const cookieConfig = {
  path: nconf.get('cookie:path'),
  maxAge: nconf.get('cookie:maxAge'),
  secure: nconf.get('cookie:secure'),
  httpOnly: nconf.get('cookie:httpOnly'),
  sameSite: nconf.get('cookie:sameSite'),
};
const clearCookieConfig = {
  path: nconf.get('cookie:path'),
  maxAge: 0,
  secure: nconf.get('cookie:secure'),
  httpOnly: nconf.get('cookie:httpOnly'),
  sameSite: nconf.get('cookie:sameSite'),
};

const cookieName = nconf.get('cookie:name');

const createRefreshToken = (user) => {
  const iatTime = Math.floor(Date.now() / 1000);
  const expTime = iatTime + nconf.get('jwt:refreshToken:expiration');
  const payload = {
    aud: nconf.get('jwt:refreshToken:audience'),
    iss: nconf.get('jwt:refreshToken:issuer'),
    sub: user.get('id'),
    iat: iatTime,
    exp: expTime,
  };
  const options = {
    algorithm: 'HS256',
  };
  return jsonwt.sign(payload, nconf.get('jwt:refreshToken:secret'), options);
};

module.exports = {
  createRefreshToken,
  cookieConfig,
  clearCookieConfig,
  cookieName,
};
