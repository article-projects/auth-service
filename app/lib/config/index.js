const path = require('path');
const nconf = require('nconf');
const configRequired = require('../../../config/nconf/config_required');
const configDefault = require('../../../config/nconf/config_default');

module.exports = () => {
  nconf.argv().env('__').file(path.join(__dirname, '../../../config/nconf/', `config_${(process.env.NODE_ENV || 'development')}.json`));
  nconf.set('NODE_ENV', nconf.get('NODE_ENV') || 'development');
  nconf.defaults(configDefault);
  nconf.required(configRequired);
};
