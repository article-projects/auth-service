module.exports = function (grunt) {
  grunt.initConfig({
    env: {
      ci: {
        NODE_ENV: 'continuous_integration',
        JEST_JUNIT_OUTPUT: './shippable/testresults/results.xml',
      },
    },
    run: {
      eslint: {
        exec: 'eslint .',
      },
      test: {
        exec: 'jest --config config/jest/jest.json',
      },
      test_unit_only: {
        exec: 'jest --config config/jest/jest_unit_tests.json',
      },
      drop_db: {
        exec: 'sequelize db:drop || true',
      },
      create_db: {
        exec: 'sequelize db:create',
      },
      init_db: {
        exec: 'sequelize db:migrate',
      },
      seed_db: {
        exec: 'sequelize db:seed:all',
      },
    },
  });

  grunt.loadNpmTasks('grunt-env');
  grunt.loadNpmTasks('grunt-run');

  grunt.registerTask('default', [
    'env:ci',
    'run:drop_db',
    'run:create_db',
    'run:init_db',
    'run:seed_db',
    'run:test',
    'run:eslint',
  ]);

  grunt.registerTask('unit_tests_only', [
    'env:ci',
    'run:test_unit_only',
  ]);
};
